Source/header switcher plugin for Notepad++
===========================================

See releases page for binary downloads: https://gitlab.com/roelschroeven-windowstools/nppcpphpp/-/releases

Build
-----

Open vs.proj\NppCppHpp.sln in Visual Studio. Select 32 or 64 target, depending
on which variant of Notepad++ you have. Build.

Install
-------

Create directory NppCppHpp in Notepad++'s plugin directory. Copy appropriate
NppCppHpp.dll (32 or 64 bit) to that directory.

Use
---

  * The long way: Plugins -> NppCppHpp -> Switch!!
  * The short way: F9