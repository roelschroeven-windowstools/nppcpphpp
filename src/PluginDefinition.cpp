//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "PluginDefinition.h"
#include "menuCmdID.h"

#include <string>
#include <shlwapi.h>

//
// The plugin data that Notepad++ needs
//
FuncItem funcItem[nbFunc];

//
// The data of Notepad++ that you can use in your plugin commands
//
NppData nppData;

//
// Initialize your plugin data here
// It will be called while plugin loading   
void pluginInit(HANDLE /*hModule*/)
{
}

//
// Here you can do the clean up, save the parameters (if any) for the next session
//
void pluginCleanUp()
{
}

//
// Initialization of your plugin commands
// You should fill your plugins commands here
void commandMenuInit()
{

    //--------------------------------------------//
    //-- STEP 3. CUSTOMIZE YOUR PLUGIN COMMANDS --//
    //--------------------------------------------//
    // with function :
    // setCommand(int index,                      // zero based number to indicate the order of command
    //            TCHAR *commandName,             // the command name that you want to see in plugin menu
    //            PFUNCPLUGINCMD functionPointer, // the symbol of function (function pointer) associated with this command. The body should be defined below. See Step 4.
    //            ShortcutKey *shortcut,          // optional. Define a shortcut to trigger this command
    //            bool check0nInit                // optional. Make this menu item be checked visually
    //            );

	ShortcutKey *shKeyF9 = new ShortcutKey;
	shKeyF9->_isAlt = false;
	shKeyF9->_isCtrl = false;
	shKeyF9->_isShift = false;
	shKeyF9->_key = VK_F9;
	setCommand(0, TEXT("Switch!!"), switchCppHpp, shKeyF9, false);
}

//
// Here you can do the clean up (especially for the shortcut)
//
void commandMenuCleanUp()
{
	// Loop over them all, delete all shortcuts that are not NULL
	for (int i = 0; i < nbFunc; ++i)
	{
		if (funcItem[i]._pShKey)
			delete funcItem[i]._pShKey;
	}
}


//
// This function help you to initialize your plugin commands
//
bool setCommand(size_t index, TCHAR *cmdName, PFUNCPLUGINCMD pFunc, ShortcutKey *sk, bool check0nInit) 
{
    if (index >= nbFunc)
        return false;

    if (!pFunc)
        return false;

    lstrcpy(funcItem[index]._itemName, cmdName);
    funcItem[index]._pFunc = pFunc;
    funcItem[index]._init2Check = check0nInit;
    funcItem[index]._pShKey = sk;

    return true;
}

//----------------------------------------------//
//-- STEP 4. DEFINE YOUR ASSOCIATED FUNCTIONS --//
//----------------------------------------------//

const wchar_t *SourceExts[] =
{
	L".c",
	L".cpp",
	L".cxx",
};

const wchar_t *HeaderExts[] =
{
	L".h",
	L".hpp",
	L".hxx",
};

bool isOneOf(const wchar_t *s, const wchar_t *keys[], int nr_keys)
{
	for (int i = 0; i < nr_keys; ++i)
	{
		if (wcscmp(s, keys[i]) == 0)
			return true;
	}
	return false;
}

void openFirstOf(const wchar_t *Directory, const wchar_t *NamePart, const wchar_t *Exts[], int NrExts)
{
	std::wstring Base = std::wstring(Directory) + L"\\" + NamePart;
	for (int i = 0; i < NrExts; ++i)
	{
		std::wstring Candidate = Base + Exts[i];
		if (PathFileExistsW(Candidate.c_str()))
		{
			::SendMessageW(nppData._nppHandle, NPPM_DOOPEN, 0, (LPARAM)Candidate.c_str());
			break;
		}
	}
}

void switchCppHpp()
{
	wchar_t Directory[2048];
	::SendMessageW(nppData._nppHandle, NPPM_GETCURRENTDIRECTORY, 0, (LPARAM)Directory);
	wchar_t NamePart[1024];
	::SendMessageW(nppData._nppHandle, NPPM_GETNAMEPART, 0, (LPARAM)NamePart);
	wchar_t ExtPart[1024];
	::SendMessageW(nppData._nppHandle, NPPM_GETEXTPART, 0, (LPARAM)ExtPart);
	OutputDebugStringW(Directory);
	OutputDebugStringW(NamePart);
	OutputDebugStringW(ExtPart);

	if (isOneOf(ExtPart, SourceExts, ARRAYSIZE(SourceExts)))
		openFirstOf(Directory, NamePart, HeaderExts, ARRAYSIZE(HeaderExts));
	else if (isOneOf(ExtPart, HeaderExts, ARRAYSIZE(HeaderExts)))
		openFirstOf(Directory, NamePart, SourceExts, ARRAYSIZE(SourceExts));
}
